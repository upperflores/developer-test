# Teste para candidatos a vaga de developer
> Essa prova consiste em testar seus conhecimentos com **HTML, CSS, JavaScript e PHP** entre outras coisas. <br>
> O conjunto de testes disponibilizado tem o prazo de ** 3 dias** para serem concluídos;

## Instruções:

1. Faça o clone do projeto;
2. Implemente o HTML/CSS do layout contido no arquivo layout/Hotsite.psd;
3. Carregue e faça a listagem dos itens (pins) na tela (ul>li) via JSON (exemplo em develop/database/locations.json);
5. Implemente uma rotina para centralizar o mapa em cada ponto dos endereços após o clique (botão ver no mapa);
6. Exibir os detalhes do local no clique do pin;

## Orientações

* Organização do projeto: Avalia a estrutura do projeto, documentação e uso de controle de versão;
* O conteúdo da página pode ser estático.<br>
* A utilização de Frameworks HTML/CSS é de livre escolha.<br>
* Valorizamos a codificação de JavaScript em OOP<br>
* Para progamadores fullstack, carregue os dados da listagem via banco de dados
* Considere a integração com Laravel ou Wordpress<br>
* Se tiver familiaridade com outros frameworks, codeigniter, cake, etc. Pode utilizar também.

### Ganhe pontos extras por:

* Desenvolver HTML semântico;
* Componentizar seu CSS;
* Utilizar algum pre-processador CSS (SASS/LESS);
* Ser fiel as especificações do layout;
* Utilizar Laravel como plataforma backend;
* Adaptação para a versão mobile;

**Boa sorte! =D**